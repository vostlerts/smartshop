<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Goods extends Model
{

    protected $fillable = [ 'name', 'short_description', 'description', 'icon' ];

    static function getItem($id)
    {
        return DB::table('goods')->where('id', $id)->get();
    }

}
