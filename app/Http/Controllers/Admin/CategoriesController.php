<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    public function index()
    {
        $items = Category::get();
        return view('admin/categories/index', [ 'items' => $items ]);
    }

    static function getNameParentCategory($id)
    {
        if ($id == null)
        {
            return 'Отсутствует';
        }
        $category = Category::where('id', $id)->get();
        return $category[0]->displayName;
    }
}
