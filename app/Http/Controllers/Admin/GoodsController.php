<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Goods;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GoodsController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		$items = Goods::all();
		return view('admin/goods/index', ['items' => $items]);
	}

	static function getCategory($id)
    {
        $category = Category::where('id', $id)->get();
        return $category[0]->displayName;
    }

	public function add()
	{
        $categories = Category::all();
		return view('admin/goods/create', ['categories' => $categories]);
	}

	public function view(Request $request, $id)
	{
		$item = Goods::find($id);
		$reviews = $item->reviews;
		$media = $item->media;
		return view('admin/goods/view', ['item' => $item, 'media' => $media, 'reviews' => $reviews]);
	}

	public function edit($id)
	{
		$item = Goods::find($id);
		$reviews = $item->reviews;
		$media = $item->media;
        $categories = Category::all();
		return view('admin/goods/edit', ['item' => $item, 'categories' => $categories, 'media' => $media, 'reviews' => $reviews]);
	}

	public function create(Request $request)
	{
	    Goods::create([
	        'title' => $request->input('title'),
	        'category' => intval($request->input('category')),
	        'description' => $request->input('description'),
	        'amount' => $request->input('amount'),
	        'path_preview' => $request->input('path_preview')
        ]);
		return redirect('/admin/goods');
	}

    public function update(Request $request, $id)
	{
		Goods::find($id)->update($request->all());
		return redirect('/admin/goods/edit/' . $id);
	}

	public function delete(Request $request, $id)
	{
		Goods::find($id)->delete();
		return redirect('/admin/goods/');
	}
}
