<?php

namespace App\Http\Controllers;

use App\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    public function index()
    {
        if (!Session::has('cart')) {
            return view('cart.index', [ 'totalCount' => 0 ]);
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        return view('cart.index', ['products' => $cart->items, 'totalCount' => count($cart->items), 'totalPrice' => $cart->totalPrice]);
    }
}
