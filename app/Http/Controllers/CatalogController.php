<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Goods;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    public function section($section)
    {
        $category = Category::find($section);
        $categories = Category::all();
        $items = [];
        foreach ($categories as $_category) {
            if ($category->displayName == $_category->displayName || $category->id == $_category->parent_category) {
                $items[] = Goods::where('category', $_category->id)->get();
            }
        }
        $_items = collect([]);
        foreach ($items as $item) {
            if ($item->isNotEmpty()) {
                if ($item->count() > 0) {
                    foreach ($item as $_item) {
                        $_items->push($_item);
                    }
                }
            }
        }
        return view('goods/index', [ 'section' => $section, 'goods' => $_items ]);
    }

    public function subsection($section, $subsection)
    {
        $category = Category::find($subsection);
        $items = Goods::where('category', $category->id)->get();
//        dd($items);
        return view('goods/index', [ 'section' => $section, 'goods' => $items ]);
    }
}
