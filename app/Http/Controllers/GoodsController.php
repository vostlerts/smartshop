<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Models\Category;
use App\Models\Goods;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class GoodsController extends Controller
{
    public function view(Request $request, $id)
    {
	    $item = Goods::find($id);
	    $category = Category::where('id', $item->category)->get();
	    $category = $category[0];
	    $reviews = $item->reviews;
	    $media = $item->media;
    	return view('goods/view', [ 'section' => $category, 'item' => $item, 'media' => $media, 'reviews' => $reviews]);
    }

    public function getAddToCart(Request $request, $id)
    {
        $product = Goods::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($product, $product->id);
        $request->session()->put('cart', $cart);
        return redirect('/bag');
    }

    public function removeFromCart(Request $request, $id)
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->remove($id);
        $request->session()->put('cart', $cart);
        return redirect('/bag');
    }
}
