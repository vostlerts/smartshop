<?php

namespace App\Http\Controllers;

use App\Goods;

use Illuminate\Http\Request;

class GoodsCreateController extends Controller
{
    public function index()
    {
        return view('goods/create');
    }

    public function create(Request $request)
    {
//        dd(request()->all());
        $this->validate($request, [
            'name' => 'required',
            'short_description' => 'required',
            'description' => 'required',
            'icon' => 'required'
        ]);
        Goods::create($request->all());
        return redirect('/goods');
    }
}
