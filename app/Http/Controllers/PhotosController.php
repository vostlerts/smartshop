<?php

namespace App\Http\Controllers;

use App\Photos;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PhotosController extends Controller
{
    
    public function index()
    {
        $photos = Photos::getItems();
        return view('photos/index', ['photos' => $photos]);
    }

}
