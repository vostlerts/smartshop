<?php

namespace App\Http\Controllers;

use App\Photos;

use Illuminate\Http\Request;

class PhotosCreateController extends Controller
{
    public function index() {
        return view('photos/create');
    }

    public function create(Request $request) {
        $this->validate($request, [
           'name' => 'required',
           'alt' => 'required',
           'title' => 'required',
           'path' => 'required'
        ]);
        Photos::create($request->all());

        return redirect('/photos');
    }
}
