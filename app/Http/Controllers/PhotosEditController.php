<?php

namespace App\Http\Controllers;

use App\Photos;

use Illuminate\Http\Request;

class PhotosEditController extends Controller
{
    public function index($good_id)
    {
        $good_item = Photos::getItemById($good_id);

        return view('photos/edit', [
            'name' => $good_item->name,
            'alt' => $good_item->alt,
            'title' => $good_item->title,
            'path' => $good_item->path
        ]);
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'alt' => 'required',
            'title' => 'required',
            'path' => 'required'
        ]);
        Photos::where('id', $request->input('id'))->update([
            'name' => $request->input('name'),
            'alt' => $request->input('alt'),
            'title' => $request->input('title'),
            'path' => $request->input('path')
        ]);
        return redirect('/photos');
    }
}
