<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class Cart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Session::has('cart')) {
            $oldCart = Session::get('cart');
            $cart = new \App\Cart($oldCart);
            $request->merge(['cart' => $cart]);
        }
        return $next($request);
    }
}
