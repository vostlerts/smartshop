<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Goods extends Model
{

    protected $fillable = [ 'name', 'short_description', 'description', 'icon', 'category' ];

    static function getItem($id)
    {
        return DB::table('goods')->where('id', $id)->get();
    }
}
