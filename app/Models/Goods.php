<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Goods extends Model
{
	public $timestamps = false;
	protected $fillable = [ 'title', 'description', 'amount', 'path_preview', 'category' ];

	static function getItem($id)
	{
		return DB::table('goods')->where('id', $id)->get();
	}

	static function getItems()
	{
		return DB::table('goods')->get();
	}

	public function reviews()
	{
		return $this->hasMany('App\Models\Reviews');
	}

	public function media()
	{
		return $this->hasMany('App\Models\Media');
	}
}
