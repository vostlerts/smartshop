<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Photos extends Model
{
    public $timestamps = false;
    protected $fillable = [ 'name', 'alt', 'title', 'path' ];

    static function getItems()
    {
        return DB::table('photos')->get();
    }

    static function getItemById($id)
    {
        return DB::table('photos')->where('id', $id)->first();
    }
}
