<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
	    $collection = collect([
	    	['name' => 'Вся продукция', 'url' => ''],
	    	['name' => 'Ноутбуки', 'url' => ''],
		    ['name' => 'Аксессуары', 'url' => '']
	    ])->all();

	    view()->composer('includes.menu', function($view) use ($collection) {
		    $view->with('items', $collection);
	    });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
