<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Category;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mac = Category::create([
            'name' => 'mac',
            'displayName' => 'Mac'
        ]);
        Category::create([
            'name' => 'macbook-pro',
            'displayName' => 'MacBook Pro',
            'parent_category' => $mac->name
        ]);
        Category::create([
            'name' => 'macbook-air',
            'displayName' => 'MacBook Air',
            'parent_category' => $mac->name
        ]);
        $iphone = Category::create([
            'name' => 'iphone',
            'displayName' => 'iPhone'
        ]);
        Category::create([
            'name' => 'iphone-x',
            'displayName' => 'iPhone X',
            'parent_category' => $iphone->name
        ]);
    }
}
