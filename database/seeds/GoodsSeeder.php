<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Goods;

class GoodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Goods::create([
            'title' => 'MacBook Pro 15" с Touch Bar, 16 ГБ, 512 ГБ, Intel Core i7, Серый космос',
            'description' => 'Новый MacBook Pro остался невероятно тонким и лёгким, но стал быстрее и мощнее. У него самый яркий экран и лучшая цветопередача среди всех ноутбуков Mac. Во все его конфигурации входят новейшие 6-ядерные процессоры и высокопроизводительные графические процессоры Radeon Pro. В клавиатуру MacBook Pro встроена панель Touch Bar, выполненная из стекла. Она поддерживает жесты Multi-Touch и позволяет быстро переходить к нужным функциям. Новый MacBook Pro создан на основе самых передовых идей. И у него есть всё для воплощения ваших.',
            'amount' => 6590.32,
            'article' => 'MR942RU/A',
            'path_preview' => 'http://cdn0.it4profit.com/resize/940x-/catalog-products/180713071433552893/180717160009348062.jpg',
            'category' => 2
        ]);
        Goods::create([
            'title' => 'MacBook Air 13", 8 ГБ, 256 ГБ, Intel Core i5, Серебристый',
            'description' => 'MacBook Air оснащён процессором Intel Core пятого поколения, до 8 ГБ памяти, портом Thunderbolt 2 и отличными встроенными приложениями. И работает без подзарядки целый день.* Он тонкий, лёгкий и достаточно прочный, чтобы брать его с собой повсюду. А его мощности хватит, чтобы справиться с любой задачей.',
            'amount' => 3239,
            'article' => 'MQD42RU/A',
            'path_preview' => 'http://cdn0.it4profit.com/resize/940x-/catalog-products/170606082407113546/171222150035062277.png',
            'category' => 3
        ]);

        Goods::create([
            'title' => 'iPhone X, 256 ГБ, Серый космос',
            'description' => 'Всю переднюю поверхность iPhone X занимает дисплей Super Retina HD с диагональю 5,8 дюйма и поддержкой технологий HDR и True Tone.* Передняя и задняя панели выполнены из самого прочного стекла, когда-либо созданного для iPhone, а рамка — из хирургической нержавеющей стали. iPhone X заряжается без проводов.** Защищён от воды и пыли.*** И оснащён двойной камерой 12 Мп с двойной оптической стабилизацией изображения для отличных снимков даже при слабом освещении. Камера TrueDepth с новой функцией портретного освещения может сделать селфи в режиме «Портрет».**** А технология Face ID позволяет одним взглядом разблокировать iPhone X. Всё это работает на A11 Bionic — самом мощном и умном процессоре iPhone. И ещё iPhone X поддерживает технологии дополненной реальности в играх и приложениях. iPhone X — это новая эра iPhone.',
            'amount' => 3619,
            'article' => 'MQAF2RM/A',
            'path_preview' => 'http://cdn0.it4profit.com/resize/940x-/catalog-products/170913073651358079/180413170152609463.png',
            'category' => 5
        ]);
    }
}
