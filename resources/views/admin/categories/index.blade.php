@extends('admin.layouts.app')

@section('content')
    <div class="row py-2">
        <a href="{{ url('/admin/categories/add') }}" class="btn btn-primary">Добавить</a>
    </div>
    <div class="row py-1">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Имя</th>
                <th scope="col">Видимое имя</th>
                <th scope="col">Родительская категория</th>
                <th scope="col">Действие</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <th scope="row">{{ $item->id }}</th>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->displayName }}</td>
                    <td>{{ \App\Http\Controllers\Admin\CategoriesController::getNameParentCategory($item->parent_category) }}</td>
                    <td>
                        <div class="btn-group">
                            <a href="{{ url('/admin/categories/edit', $item->id) }}" class="btn btn-sm btn-outline-secondary">{{ __('Edit') }}</a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection