@extends('admin.layouts.app')

@section('content')
    <div class="py-5">
        <h4 class="mb-3">Редактирование товара #{{ $item->id }}</h4>
        <form class="needs-validation" action="{{ url('/admin/goods/update', $item->id) }}" method="POST">
            @method('PUT')
            @csrf()
            <div class="mb-3">
                <label for="username">Название</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="title" name="title" placeholder="Название" required="" value="{{ $item->title }}">
                </div>
            </div>
            <div class="mb-3">
                <label for="username">Описание</label>
                <div class="input-group">
                    <textarea class="form-control" name="description" placeholder="Описание товара">{{ $item->description }}</textarea>
                </div>
            </div>
            <div class="mb-3">
                <label for="username">Категория</label>
                <div class="input-group">
                    <select class="form-control" name="category">
                        @foreach($categories as $category)
                            <option value="{{ $category['id'] }}" {{ (($category['id'] == $item->category) ? 'selected="selected"' : '') }}>{{ $category['displayName'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="mb-3">
                <div class="row">
                    <div class="col">
                        <label for="username">Цена</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">$</span>
                            </div>
                            <input type="text" class="form-control" name="amount" placeholder="Цена" required="" value="{{ $item->amount }}">
                        </div>
                    </div>
                    <div class="col">
                        <label for="username">Превью</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text">URL</span>
                            </div>
                            <input type="text" class="form-control" name="path_preview" placeholder="Путь на внешний источник" required="" value="{{ $item->path_preview }}">
                        </div>
                    </div>
                </div>
            </div>
            <hr class="mb-4">
            <button class="btn btn-primary btn-lg btn-block" type="submit">{{ __('Update') }}</button>
        </form>
        <br>
        <form action="{{ url('/admin/goods/delete', $item->id) }}" method="POST">
            @method('DELETE')
            @csrf()
            <button class="btn btn-outline-danger btn-lg btn-block" type="submit">{{ __('Delete') }}</button>
        </form>
    </div>
@endsection