@extends('admin.layouts.app')

@section('content')
    <div class="row py-2">
        <a href="{{ url('/admin/goods/add') }}" class="btn btn-primary">Добавить</a>
    </div>
    <div class="row py-1">
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Название</th>
                <th scope="col">Категория</th>
                <th scope="col">Цена</th>
                <th scope="col">Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($items as $item)
                <tr>
                    <th scope="row">{{ $item->id }}</th>
                    <td>{{ $item->title }}</td>
                    <td>{{ \App\Http\Controllers\Admin\GoodsController::getCategory($item->category) }}</td>
                    <td>{{ $item->amount }}$</td>
                    <td>
                        <div class="btn-group">
                            <a href="{{ url('/admin/goods/view', $item->id) }}" class="btn btn-sm btn-outline-secondary">{{ __('More detail') }}</a>
                            <a href="{{ url('/admin/goods/edit', $item->id) }}" class="btn btn-sm btn-outline-secondary">{{ __('Edit') }}</a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
