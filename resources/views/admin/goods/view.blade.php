@extends('admin.layouts.app')

@section('content')
    <div class="container">
        <div class="row py-5">
            <div class="col-sm-12 col-md-4">
                <img style="max-width:100%;" src="{{ $item->path_preview }}" />
            </div>
            <div class="col-sm-12 col-md-8">
                <h2 class="title">{{ $item->title }}</h2>
                <h3 class="main-price">${{ $item->amount }}</h3>
                <div class="description py-5">{{ $item->description }}</div>
                <div class="btn-group py-5">
                    <a href="{{ url('/admin/goods/edit', $item->id) }}" class="btn btn-secondary">{{ __('Edit') }}</a>
                </div>
            </div>
        </div>
        @if(count($media) > 0)
            <hr>
            <div class="row py-2">
                <div class="col-12">
                </div>
            </div>
        @endif

        <hr>
        <div class="row py-2">
            <div class="col-12">
                <h3>Отзывы</h3>
                @if(count($reviews) > 0)
                    @foreach($reviews as $review)
                        <div class="row highlight py-1">
                            <div class="col-1">
                                <img class="mr-3" data-src="holder.js/64x64" alt="64x64" style="width: 64px; height: 64px;" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%2264%22%20height%3D%2264%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2064%2064%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_164d5d2b8a9%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A10pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_164d5d2b8a9%22%3E%3Crect%20width%3D%2264%22%20height%3D%2264%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2213.5546875%22%20y%3D%2236.5%22%3E64x64%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" data-holder-rendered="true">
                            </div>
                            <div class="col-11">
                                <div class="py-2">
                                    @if($review->author != NULL)
                                        <h5>{{ $review->author }}</h5>
                                    @else
                                        <h5>{{ __('Guest') }}</h5>
                                    @endif
                                    <small class="text-muted">Дата: {{ $review->create_at }}</small>
                                    <div class="py-2">
                                        {{ $review->text }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="text-muted">Отзывов еще нет.</div>
                @endif
            </div>
        </div>
    </div>
@endsection