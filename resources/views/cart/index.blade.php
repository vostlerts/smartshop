@extends('layouts.app')

@section('title')
    Корзина
@endsection

@section('content')
    <br>
    <br>
    <br>
    <br>
    <div class="container cart">
        <div class="header">
            <h1>
                <span>Ваша корзина</span>
                <span>Товаров в корзине: {{ $totalCount }}</span>
            </h1>
        </div>

        @if(Session::has('cart'))
            <div class="list-products">
                @foreach($products as $key => $product)
                    <div class="item row">
                        <div class="col-lg-9">
                            <img class="pull-left" src="{{ $product['item']['path_preview'] }}">
                            <div class="wrapper">
                                <div class="header">{{ $product['item']['title'] }}</div>
                                <div class="text">
                                    <span>{{ $product['item']['title'] }}</span>
                                    <br>
                                    <span>Артикул: {{ $product['item']['article'] }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <a href="{{ route('goods.removeFromCart', [ 'id' => $key ]) }}" class="pull-right button-delete">
                                x
                            </a>
                            <div class="price">
                                <div class="header">Цена:</div>
                                <span>
                                    <span>{{ $product['item']['amount'] }}</span>
                                    <span>руб.</span>
                                </span>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="text-right total-price">
                <div class="wrapper">
                    <div class="header">Общая стоимость</div>
                    <div class="cost">
                        <span>{{ $totalPrice }}</span>
                        <span>руб.</span>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection