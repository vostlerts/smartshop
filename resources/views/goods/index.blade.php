@extends('layouts.app')

@section('content')
    @if($section == 'mac')
        <nav id="chapternav" class="chapternav theme-dark" data-analytics-region="family browser">
            <div class="chapternav-wrapper">
                <ul class="chapternav-items">
                    <li class="chapternav-item chapternav-item-macbook macbook">
                        <a class="chapternav-link" href="{{ url('/catalog/mac/macbook/') }}">
                            <figure class="chapternav-icon"></figure>
                            <span class="chapternav-label">MacBook</span>
                        </a>
                    </li><!--
				--><li class="chapternav-item chapternav-item-macbook-air macbook-air">
                        <a class="chapternav-link" href="{{ url('/catalog/mac/macbook-air/') }}">
                            <figure class="chapternav-icon"></figure>
                            <span class="chapternav-label">MacBook Air</span>
                        </a>
                    </li><!--
				--><li class="chapternav-item chapternav-item-macbook-pro macbook-pro">
                        <a class="chapternav-link" href="{{ url('/catalog/mac/macbook-pro/') }}">
                            <figure class="chapternav-icon"></figure>
                            <span class="chapternav-label">MacBook Pro</span>
                            <span class="chapternav-new">New</span>
                        </a>
                    </li><!--
				--><li class="chapternav-item chapternav-item-imac imac">
                        <a class="chapternav-link" href="{{ url('/catalog/mac/imac/') }}">
                            <figure class="chapternav-icon"></figure>
                            <span class="chapternav-label">iMac</span>
                        </a>
                    </li><!--
				--><li class="chapternav-item chapternav-item-imac-pro imac-pro">
                        <a class="chapternav-link" href="{{ url('/catalog/mac/imac-pro/') }}">
                            <figure class="chapternav-icon"></figure>
                            <span class="chapternav-label">iMac Pro</span>
                        </a>
                    </li><!--
				--><li class="chapternav-item chapternav-item-mac-pro mac-pro">
                        <a class="chapternav-link" href="{{ url('/catalog/mac/mac-pro/') }}">
                            <figure class="chapternav-icon"></figure>
                            <span class="chapternav-label">Mac Pro</span>
                        </a>
                    </li><!--
				--><li class="chapternav-item chapternav-item-mac-mini mini">
                        <a class="chapternav-link" href="{{ url('/catalog/mac/mac-mini/') }}">
                            <figure class="chapternav-icon"></figure>
                            <span class="chapternav-label">Mac mini</span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    @elseif($section == 'iphone')
        <nav id="chapternav" class="chapternav theme-dark" data-analytics-region="family browser">
            <div class="chapternav-wrapper">
                <ul class="chapternav-items">
                    <li class="chapternav-item chapternav-item-overview">
                        <a class="chapternav-link" href="{{ url('/catalog/iphone/iphone-x/') }}">
                            <figure class="chapternav-icon"></figure>
                            <span class="chapternav-label">iPhone X</span>
                        </a>
                    </li>
                    <li class="chapternav-item chapternav-item-iphone-8">
                        <a class="chapternav-link" href="{{ url('/catalog/iphone/iphone-8/') }}">
                            <figure class="chapternav-icon"></figure>
                            <span class="chapternav-label">iPhone 8</span>
                        </a>
                    </li>
                    <li class="chapternav-item chapternav-item-iphone-7">
                        <a class="chapternav-link" href="{{ url('/catalog/iphone/iphone-7/') }}">
                            <figure class="chapternav-icon"></figure>
                            <span class="chapternav-label">iPhone 7</span>
                        </a>
                    </li>
                    <li class="chapternav-item chapternav-item-iphone-6s">
                        <a class="chapternav-link" href="{{ url('/catalog/iphone/iphone-6s/') }}">
                            <figure class="chapternav-icon"></figure>
                            <span class="chapternav-label">iPhone 6s</span>
                        </a>
                    </li>
                    <li class="chapternav-item chapternav-item-iphone-se">
                        <a class="chapternav-link" href="{{ url('/catalog/iphone/iphone-se/') }}">
                            <figure class="chapternav-icon"></figure>
                            <span class="chapternav-label">iPhone SE</span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    @endif
    <br>
    <section class="container">
        <div class="row">
            @foreach($goods as $good)
                <div class="col-md-3 good-item">
                    <a href="{{ url('/goods/view', $good->id) }}">
                        <div class="img">
                            <div class="img-wrapper">
                                <img src="{{ $good->path_preview }}">
                            </div>
                        </div>
                        <div class="captain">
                            <div class="header">{{ $good->title }}</div>
                            <div class="text">Артикул: {{ $good->article }}</div>
                        </div>
                        <div class="price">
                            <span>
                                <span>{{ $good->amount }}</span>
                                <span>руб.</span>
                            </span>
                        </div>
                        <a href="{{ route('goods.addToCart', [ 'id' => $good->id ]) }}" class="button">
                            <svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 20 21" style="enable-background:new 0 0 20 21" xml:space="preserve">
                                <style type="text/css">.st0{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}</style>
                                <path class="st0" d="M16,13V9c0-0.7-0.4-1.4-1-1.7V10h-2V7H5v3H3V7.3C2.4,7.6,2,8.3,2,9v8h10v2H2H0v-2V7c0-1.1,0.9-2,2-2h1h0.1C3.6,2.2,6,0,9,0c3,0,5.4,2.2,5.9,5H15h1c1.1,0,2,0.9,2,2v6H16z M9,2C7.1,2,5.6,3.3,5.1,5h7.7C12.4,3.3,10.9,2,9,2z"></path>
                                <path class="st0" d="M20,19h-2v2h-2v-2h-2v-2h2v-2h2v2h2V19z"></path>
                            </svg>
                        </a>
                    </a>
                </div>
            @endforeach
        </div>
    </section>
@endsection