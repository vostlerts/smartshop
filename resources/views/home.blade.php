@extends('layouts.app')

@section('content')
    <section class="homepage-section section-heroes">
        <ul class="homepage-section-positions hero-positions heroes-count-3" data-heroes-count="3">
            <li id="section-1-hero-position-1" class="homepage-section-item hero-position-1 hero-position hero-position-1">
                <div class="hero iphone-x hero-iphone-x promo-managed-unit promo-managed-unit-iphone-x">
                    <div class="unit-wrapper">
                        <a href="/iphone-x/" class="unit-link" tabindex="0">
                            <span class="visuallyhidden unit-link-vo">iPhone X. Say hello to the future. </span>
                        </a>
                        <div class="unit-copy-wrapper">
                            <h2 class="headline" aria-hidden="true">iPhone X</h2>
                            <h3 class="subhead" aria-hidden="true">Say hello to the future.</h3>
                        </div>
                        <div class="unit-image-wrapper image-constraints-full">
                            <figure class="unit-image unit-image-iphone-x-iphone-x"></figure>
                        </div>
                    </div>
                    <div class="links cta">
                        <a href="{{ url('/catalog/iphone/iphone-x/') }}" class="more">Купить</a>
                    </div>
                </div>
            </li>
            <li id="section-1-hero-position-2" class="homepage-section-item hero-position-2 hero-position hero-position-2">
                <div class="hero ipad hero-ipad promo-managed-unit promo-managed-unit-ipad">
                    <div class="unit-wrapper">
                        <a href="/ipad-9.7/" class="unit-link" tabindex="0">
                            <span class="visuallyhidden unit-link-vo">iPad. Like a computer. Unlike any computer. Now supports Apple Pencil. </span>
                        </a>
                        <div class="unit-copy-wrapper">
                            <h2 class="headline" aria-hidden="true">iPad</h2>
                            <h3 class="subhead" aria-hidden="true">Like a computer. Unlike any computer.</h3>
                            <p class="visuallyhidden" aria-hidden="true">Now supports Apple Pencil.</p>
                        </div>
                        <div class="unit-image-wrapper image-constraints-full">
                            <figure class="unit-image unit-image-ipad-ipad"></figure>
                        </div>
                    </div>
                    <div class="links cta">
                        <a href="{{ url('/catalog/ipad/ipad-9.7/') }}" class="more">Купить</a>
                    </div>
                </div>
            </li>
            <li id="section-1-hero-position-3" class="homepage-section-item hero-position-3 hero-position hero-position-3">
                <div class="hero macbook-pro hero-macbook-pro promo-managed-unit promo-managed-unit-macbook-pro">
                    <div class="unit-wrapper theme-dark">
                        <a href="/macbook-pro/" class="unit-link" tabindex="0">
                            <span class="visuallyhidden unit-link-vo">MacBook Pro. More power. More performance. More&nbsp;pro. </span>
                        </a>
                        <div class="unit-copy-wrapper">
                            <h2 class="headline" aria-hidden="true">MacBook Pro</h2>
                            <h3 class="subhead" aria-hidden="true">More power. More performance. More&nbsp;pro.</h3>
                        </div>
                        <div class="unit-image-wrapper image-constraints-full">
                            <figure class="unit-image unit-image-macbook-pro-macbook-pro-performance"></figure>
                        </div>
                    </div>
                    <div class="links cta">
                        <a href="{{ url('/catalog/mac/macbook-pro/') }}" class="more">Купить</a>
                    </div>
                </div>
            </li>
        </ul>
    </section>
    <section class="homepage-section section-promos" data-analytics-region="promo" data-section-content-type="promos" data-analytics-activitymap-region-id="promo">
        <ul class="homepage-section-positions promo-positions promos-count-6" data-promos-count="6">
            <li id="section-3-promo-position-5" class="homepage-section-item promo-position-5 promo-position promo-position-5">
                <div class="promo airpods promo-airpods promo-managed-unit promo-managed-unit-airpods" data-promo-id="section-3-promo-airpods" data-content-unit="airpods" data-analytics-section-engagement="name:promo-airpods">
                    <div class="unit-wrapper">
                        <a href="/airpods/" class="unit-link" tabindex="0">
																			<span class="visuallyhidden unit-link-vo">AirPods. Wireless. Effortless. Magical.
																				Learn more
																				Buy
																			. </span>
                        </a>
                        <div class="unit-copy-wrapper">
                            <h4 class="headline" aria-hidden="true">AirPods</h4>
                            <h5 class="subhead" aria-hidden="true">Wireless. Effortless. Magical.</h5>
                            <div class="links typography-body-tight" aria-hidden="true">
                                <a href="{{ url('/catalog/airpods/') }}" class="more">Купить</a>
                            </div>
                        </div>
                        <div class="unit-image-wrapper image-constraints-partial">
                            <figure class="unit-image unit-image-airpods"></figure>
                        </div>
                    </div>
                </div>
            </li>
            <li id="section-3-promo-position-6" class="homepage-section-item promo-position-6 promo-position promo-position-6">
                <div class="promo watch-series-3 promo-watch-series-3 promo-managed-unit promo-managed-unit-watch-series-3" data-promo-id="section-3-promo-watch-series-3" data-content-unit="watch-series-3" data-analytics-section-engagement="name:promo-watch-series-3">
                    <div class="unit-wrapper">
                        <a href="{{ url('/catalog/watch/watch-3/') }}" class="unit-link" tabindex="0"></a>
                        <div class="unit-copy-wrapper">
                            <h4 class="headline logo-image logo-image-watch-series-3-logo" aria-hidden="true"><span class="visuallyhidden">Apple Watch Series 3</span></h4>
                            <h5 class="subhead" aria-hidden="true">The freedom of cellular.</h5>
                            <div class="links typography-body-tight" aria-hidden="true">
                                <a href="{{ url('/catalog/watch/watch-3/') }}" class="more">Купить</a>
                            </div>
                        </div>
                        <div class="unit-image-wrapper image-constraints-full">
                            <figure class="unit-image unit-image-watch-series-3"></figure>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </section>
@endsection
