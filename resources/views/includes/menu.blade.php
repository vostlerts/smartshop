<div class="list-group">
    <li class="list-group-item text-center text-uppercase"><b>Навигация</b></li>
    @foreach($items as $item)
        <a href="{{ $item['url'] }}" class="list-group-item list-group-item-action">{{ $item['name'] }}</a>
    @endforeach
</div>