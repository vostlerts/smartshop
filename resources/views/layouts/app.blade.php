<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/wss/fonts.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap-grid.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body class="wrapper">
<nav class="nav">
    <div class="content">
        <ul class="nav-list">
            <li class="nav-item apple">
                <a class="nav-link apple" href="/">
                    <span class="text">Apple</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/catalog/mac/') }}">
                    <span>Mac</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/catalog/ipad/') }}">
                    <span>iPad</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/catalog/iphone/') }}">
                    <span>iPhone</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/catalog/watch/') }}">
                    <span>Watch</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/catalog/tv/') }}">
                    <span>TV</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/catalog/accessories/') }}">
                    <span>Аксессуары</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/catalog/special-offers/') }}">
                    <span>Акции</span>
                </a>
            </li>
            {{--<li class="nav-item" role="search">--}}
                {{--<a id="link-search" class="nav-link search" href="/us/search" aria-label="Search apple.com" role="button" aria-haspopup="true"></a>--}}
            {{--</li>--}}
            <li class="nav-item">
                <a id="link-bag" class="nav-link bag" href="{{ url('/bag') }}">
                    @if(Session::has('cart'))
                        <span class="ac-gn-bag-badge" aria-hidden="true"></span>
                    @endif
                </a>
            </li>
        </ul>
    </div>
</nav>
<main>
    @yield('content')
</main>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
</body>
</html>