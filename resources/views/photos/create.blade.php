@extends('layouts.app')

@section('content')

    @if($errors->all())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="create">
        @csrf
        <input type="text" class="form-control" name="name">
        <input type="text" class="form-control" name="alt">
        <input type="text" class="form-control" name="title">
        <input type="text" class="form-control" name="path">
        <button type="submit">Send</button>
    </form>
@endsection