@extends('layouts.app')

@section('content')
    @if($errors->all())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="">
        @method('PATCH')
        @csrf
        <input type="text" class="form-control" name="name" value="{{ $name }}">
        <input type="text" class="form-control" name="alt" value="{{ $alt }}">
        <input type="text" class="form-control" name="title" value="{{ $title }}">
        <input type="text" class="form-control" name="path" value="{{ $path }}">
        <button type="submit">Save</button>
    </form>
@endsection