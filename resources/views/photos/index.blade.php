@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Photos</div>

                <div class="card-body">
                    @foreach($photos as $photo)
                        <section class="image">
                            <span>{{ $photo->name }}</span>
                            <img width="200" height="200" src="{{$photo->path}}" alt="{{$photo->alt}}" title="{{$photo->title}}">
                        </section>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection