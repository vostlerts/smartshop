<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/add-to-cart/{id}', [
    'uses' => 'GoodsController@getAddToCart',
    'as' => 'goods.addToCart'
]);

Route::get('/remove-from-cart/{id}', [
    'uses' => 'GoodsController@removeFromCart',
    'as' => 'goods.removeFromCart'
]);

Route::get('/bag', 'CartController@index');

// Guests and Users routes
Route::redirect('/catalog', '/');
Route::get('/catalog/{section}', 'CatalogController@section');
Route::get('/catalog/{section}/{subsection}', 'CatalogController@subsection');
Route::prefix('goods')->group(function() {
	Route::get('/view/{good_id}', 'GoodsController@view');
});

// Admin routes
Route::namespace('Admin')->prefix('admin')->group(function() {

	Route::redirect('/dashboard', true);
	Route::get('/dashboard', 'DashboardController@index');

	Route::prefix('goods')->group(function() {
		Route::get('/', 'GoodsController@index');
		Route::get('/add', 'GoodsController@add');
		Route::post('/create', 'GoodsController@create');
		Route::get('/view/{good_id}', 'GoodsController@view');
		Route::get('/edit/{good_id}', 'GoodsController@edit');
		Route::put('/update/{good_id}', 'GoodsController@update');
		Route::delete('/delete/{good_id}', 'GoodsController@delete');
	});

	Route::prefix('categories')->group(function() {
	    Route::get('/', 'CategoriesController@index');
	    Route::get('/add', 'CategoriesController@add');
	    Route::get('/edit/{category_id}', 'CategoriesController@edit');
        Route::put('/update/{good_id}', 'CategoriesController@update');
        Route::delete('/delete/{good_id}', 'CategoriesController@delete');
    });
});